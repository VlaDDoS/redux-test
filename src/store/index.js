import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { cashReducer } from './reducers/cashReducer';
import { customerReducer } from './reducers/customerReducer';

const rootReducer = combineReducers({
  cash: cashReducer,
  customers: customerReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
