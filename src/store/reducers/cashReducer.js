const ADD_CASH = 'ADD_CASH';
const GET_CASH = 'GET_CASH';

const initialState = {
  cash: 0,
};

const getMyCash = (state, action) => {
  if (state.cash < action.payload) {
    alert('Не достаточно средств на счету');
    return state;
  }

  return { ...state, cash: state.cash - action.payload };
};

const addMyCash = (state, action) => {
  return { ...state, cash: state.cash + action.payload };
};

export const cashReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CASH:
      return addMyCash(state, action);
    case GET_CASH:
      return getMyCash(state, action);
    default:
      return state;
  }
};

export const addCashAction = (payload) => ({ type: ADD_CASH, payload });
export const getCashAction = (payload) => ({ type: GET_CASH, payload });
