import { useDispatch, useSelector } from 'react-redux';
import { fetchCustomers } from './API/customers/fetchCustomers';
import './App.css';
import { addCashAction, getCashAction } from './store/reducers/cashReducer';
import {
  addCustomerAction,
  removeCustomerAction,
} from './store/reducers/customerReducer';

export const App = () => {
  const cash = useSelector((state) => state.cash.cash);
  const customers = useSelector((state) => state.customers.customers);
  const dispatch = useDispatch();

  const addCash = (money) => {
    dispatch(addCashAction(money));
  };

  const getCash = (money) => {
    dispatch(getCashAction(money));
  };

  const getAllCustomers = () => {
    dispatch(fetchCustomers());
  };

  const addCustomer = (userName) => {
    const user = {
      id: Date.now(),
      name: userName,
    };

    dispatch(addCustomerAction(user));
  };

  const removeCustomer = (userId) => {
    dispatch(removeCustomerAction(userId));
  };

  return (
    <div className="App">
      <h1 className="title">Счёт: {cash}</h1>

      <div className="btns">
        <button onClick={() => addCash(+prompt('Сколько?'))}>
          Пополнить счёт
        </button>
        {cash ? (
          <button onClick={() => getCash(+prompt('Сколько?'))}>
            Снять со счёта
          </button>
        ) : null}

        <button onClick={() => addCustomer(prompt('Введите имя', 'Вася'))}>
          Добавить пользователя
        </button>

        <button onClick={getAllCustomers}>Загрузить пользователей</button>
      </div>

      <div className="customers">
        {!customers.length ? (
          <h1 className="title">Клиенты отсутствуют!</h1>
        ) : (
          customers.map((user) => (
            <div className="user" key={user.id}>
              {user.name}
              <button onClick={() => removeCustomer(user.id)}>X</button>
            </div>
          ))
        )}
      </div>
    </div>
  );
};

export default App;
